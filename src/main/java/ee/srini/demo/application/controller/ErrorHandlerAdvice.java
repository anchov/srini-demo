package ee.srini.demo.application.controller;

import ee.srini.demo.feature.authentication.LoginException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.Locale;

@Log4j2
@RestControllerAdvice
public class ErrorHandlerAdvice {

    private MessageSource messageSource;

    @Autowired
    public ErrorHandlerAdvice(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ErrorResponse constraintViolationHandler(ConstraintViolationException e, Locale locale) {
        log.info("Error: " + e.getMessage());
        String message = getErrorMessage("error.invalid.input", locale);
        return ErrorResponse.builder()
                .error(message)
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorResponse methodArgumentNotValidHandler(MethodArgumentNotValidException e, Locale locale) {
        log.info("Error: " + e.getMessage());
        String message = getErrorMessage("error.invalid.input", locale);
        return ErrorResponse.builder()
                .error(message)
                .build();
    }

    private String getErrorMessage(String code, Locale locale) {
        return messageSource.getMessage(code, null, locale);
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    public ErrorResponse accessDeniedHandler(AccessDeniedException e, Locale locale) {
        log.info("Error: " + e.getMessage());
        return ErrorResponse.builder()
                .error(getErrorMessage("error.access.denied", locale))
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(LoginException.class)
    public ErrorResponse loginExceptionHandler(LoginException e, Locale locale) {
        log.info("Error: " + e.getMessage());
        return ErrorResponse.builder()
                .error(getErrorMessage("error.login", locale))
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    public ErrorResponse defaultExceptionHandler(Exception e, Locale locale) {
        log.info("Error: " + e.getMessage());
        return ErrorResponse.builder()
                .error(getErrorMessage("error.general", locale))
                .build();
    }
}