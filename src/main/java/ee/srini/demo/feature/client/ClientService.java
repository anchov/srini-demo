package ee.srini.demo.feature.client;

import ee.srini.demo.feature.client.data.ClientEntity;
import ee.srini.demo.feature.client.data.ClientRepository;
import ee.srini.demo.feature.user.data.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ClientService {

    private ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    ClientDto getClient(int clientId, int userId) {
        Optional<ClientEntity> client = clientRepository.findByIdAndUserId(clientId, userId);
        return mapClientToDto(client.orElseThrow(() -> new RuntimeException("Client not found")));
    }

    List<ClientDto> getUsersClients(int userId) {
        List<ClientEntity> byUserId = clientRepository.findByUserId(userId);
        return byUserId.stream().map(this::mapClientToDto).collect(Collectors.toList());
    }

    private ClientDto mapClientToDto(ClientEntity client) {
        return ClientDto.builder()
                .id(client.getId())
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .email(client.getEmail())
                .address(client.getAddress())
                .country(client.getCountry())
                .build();
    }

    void createClient(ClientDto clientDto, UserEntity userWhoCreates) {
        clientRepository.save(mapClientDtoToEntity(clientDto, userWhoCreates));
    }

    private ClientEntity mapClientDtoToEntity(ClientDto client, UserEntity user) {
        return ClientEntity.builder()
                .firstName(client.getFirstName())
                .lastName(client.getLastName())
                .email(client.getEmail())
                .address(client.getAddress())
                .country(client.getCountry())
                .user(user)
                .build();
    }

    ClientDto updateClient(ClientDto clientDto, UserEntity userWhoUpdates) {
        Optional<ClientEntity> client = clientRepository.findByIdAndUserId(clientDto.getId(), userWhoUpdates.getId());
        if (client.isPresent()) {
            clientRepository.save(updateClientEntityFromDto(client.get(), clientDto, userWhoUpdates));
            return clientDto;
        }
        throw new RuntimeException("Invalid client for update.");
    }

    private ClientEntity updateClientEntityFromDto(ClientEntity client, ClientDto clientDto, UserEntity user) {
        client.setFirstName(clientDto.getFirstName());
        client.setLastName(clientDto.getLastName());
        client.setEmail(clientDto.getEmail());
        client.setAddress(clientDto.getAddress());
        client.setCountry(clientDto.getCountry());
        client.setUser(user);
        return client;
    }
}
