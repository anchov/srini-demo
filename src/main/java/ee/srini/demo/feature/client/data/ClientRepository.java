package ee.srini.demo.feature.client.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends CrudRepository<ClientEntity, Integer> {
    List<ClientEntity> findByUserId(int managingUserId);

    Optional<ClientEntity> findByIdAndUserId(int clientId, int managingUserId);
}
