package ee.srini.demo.feature.country.data;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "countries", schema = "demo")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CountryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String country;
}