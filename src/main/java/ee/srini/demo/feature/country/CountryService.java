package ee.srini.demo.feature.country;

import ee.srini.demo.feature.country.data.CountryEntity;
import ee.srini.demo.feature.country.data.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CountryService {

    private CountryRepository countryRepository;

    @Autowired
    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    List<String> getCountries() {
        List<CountryEntity> countries = countryRepository.findAll();
        return countries.stream().map(country -> country.getCountry()).collect(Collectors.toList());
    }
}
