package ee.srini.demo.feature.authentication;

import ee.srini.demo.application.security.DemoUserPrincipal;
import ee.srini.demo.feature.user.data.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

    private AuthenticationManager authenticationManager;

    @Autowired
    public AuthenticationService(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    Boolean login(LoginDto loginDto) {
        if (isAuthenticated()) {
            throw new RuntimeException("Already authenticated");
        }
        UsernamePasswordAuthenticationToken authenticationTokenRequest = new
                UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword());
        try {
            Authentication authentication = this.authenticationManager.authenticate(authenticationTokenRequest);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return true;
        } catch (Exception ex) {
            throw new LoginException("Login failed");
        }
    }

    private boolean isAuthenticated() {
        Object principal = getPrincipal();
        if (principal instanceof DemoUserPrincipal) {
            DemoUserPrincipal userDetails = (DemoUserPrincipal) principal;
            return userDetails.getUser() != null;
        }
        return false;
    }

    private Object getPrincipal() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public UserEntity getAuthenticatedUser() {
        if (isAuthenticated()) {
            DemoUserPrincipal demoUserPrincipal = (DemoUserPrincipal) getPrincipal();
            return demoUserPrincipal.getUser();
        }
        throw new RuntimeException("No authenticated user found.");
    }

}
