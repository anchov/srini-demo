package ee.srini.demo.feature.client;

import ee.srini.demo.feature.client.data.ClientEntity;
import ee.srini.demo.feature.client.data.ClientRepository;
import ee.srini.demo.feature.user.data.UserEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 * This is a single test class for demonstration purpose.
 * Write tests/getting good covereage was ignored because of time constraints.
 */
@ExtendWith(MockitoExtension.class)
class ClientServiceTest {

    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientService clientService;

    @Test
    void shouldUpdateClient() {
        ClientEntity clientEntityToUpdate = ClientEntity.builder()
                .id(1)
                .build();
        UserEntity userWhoUpdates = UserEntity.builder()
                .id(2)
                .build();
        ClientDto inputClientDto = getInputClientDto();
        Mockito.when(clientRepository.findByIdAndUserId(inputClientDto.getId(), userWhoUpdates.getId())).thenReturn(Optional.of(clientEntityToUpdate));

        clientService.updateClient(inputClientDto, userWhoUpdates);
        assertEquals(inputClientDto.getFirstName(), clientEntityToUpdate.getFirstName());
        assertEquals(inputClientDto.getLastName(), clientEntityToUpdate.getLastName());
        assertEquals(inputClientDto.getEmail(), clientEntityToUpdate.getEmail());
        assertEquals(inputClientDto.getAddress(), clientEntityToUpdate.getAddress());
        assertEquals(inputClientDto.getCountry(), clientEntityToUpdate.getCountry());
        assertEquals(userWhoUpdates.getId(), clientEntityToUpdate.getUser().getId());
    }

    private ClientDto getInputClientDto() {
        return ClientDto.builder()
                .id(1)
                .firstName("testName")
                .lastName("testNameLast")
                .email("test@test.ee")
                .address("address")
                .country("country")
                .build();
    }
}