import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientCreateComponent} from "./client/client-create/client-create.component";
import {ClientEditComponent} from "./client/client-edit/client-edit.component";
import {LoginComponent} from "./authentication/login/login.component";
import {ClientListComponent} from "./client/client-list/client-list.component";


const routes: Routes = [
  {path: 'clients', component: ClientListComponent},
  {path: 'clients/create', component: ClientCreateComponent},
  {path: 'clients/edit/:clientId', component: ClientEditComponent},
  {path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
