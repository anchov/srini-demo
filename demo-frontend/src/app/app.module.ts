import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ClientCreateComponent} from "./client/client-create/client-create.component";
import {ClientEditComponent} from "./client/client-edit/client-edit.component";
import {LoginComponent} from "./authentication/login/login.component";
import {FormsModule} from "@angular/forms";
import {ClientListComponent} from "./client/client-list/client-list.component";
import {NotificationComponent} from "./app-core/notification/notification.component";
import {HttpErrorInterceptor} from "./app-core/http-error-interceptor.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ClientListComponent,
    ClientEditComponent,
    ClientCreateComponent,
    NotificationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
