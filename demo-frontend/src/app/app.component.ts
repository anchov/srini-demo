import {Component, OnInit} from '@angular/core';
import {NavigationStart, Router} from "@angular/router";
import {AuthenticationService} from "./authentication/authentication.service";
import {NotificationService} from "./app-core/notification/notification.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Srini Demo';

  constructor(private authenticationService: AuthenticationService,
              private notificationService: NotificationService,
              private router: Router) {
  }

  logout() {
    this.authenticationService.logout().subscribe(
      () => this.router.navigateByUrl('/login')
    );
  }

  ngOnInit(): void {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.notificationService.clearError();
      }
    });
    this.router.navigateByUrl('/login');
  }
}
