import { Component, Input } from "@angular/core";
import { NotificationService } from "./notification.service";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
})
export class NotificationComponent {
  error$ = this.notificationService.getError$();

  constructor(private readonly notificationService: NotificationService) {}
}
