import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BackendApiService {

  constructor(private httpClient: HttpClient) {
  }

  public get<T>(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.httpClient
      .get<T>(`${environment.backendApiUrl}${path}`, {params});
  }

  public put<T>(path: string, body: object = {}): Observable<any> {
    return this.httpClient
      .put<T>(`${environment.backendApiUrl}${path}`, body);
  }

  public post<T>(path: string, body: object = {}): Observable<any> {
    return this.httpClient
      .post<T>(`${environment.backendApiUrl}${path}`, body);
  }

  public delete<T>(path): Observable<any> {
    return this.httpClient
      .delete<T>(`${environment.backendApiUrl}${path}`);
  }
}
