import {Component, OnInit} from '@angular/core';
import {Client} from "../client";
import {ActivatedRoute, Router} from "@angular/router";
import {CountryService} from "../../country/country.service";
import {ClientService} from "../client.service";
import {NotificationService} from "../../app-core/notification/notification.service";


@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.css']
})
export class ClientEditComponent implements OnInit {

  client = <Client>{};
  availableCountries: String[];

  constructor(private clientService: ClientService,
              private countryService: CountryService,
              private notificationService: NotificationService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  editClient() {
    this.clientService.edit(this.client).subscribe(
      success => {
        this.router.navigateByUrl('/clients');
      },
      error => {}
    );
  }

  ngOnInit() {
    this.notificationService.clearError();
    this.route.params.subscribe(params =>
      this.clientService.get(params['clientId']).subscribe(
        client => this.client = client
      )
    );
    this.countryService.getAll().subscribe(data => {
      this.availableCountries = data;
    });
  }
}
