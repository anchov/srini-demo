import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Client} from "../client";
import {CountryService} from "../../country/country.service";
import {ClientService} from "../client.service";
import {NotificationService} from "../../app-core/notification/notification.service";


@Component({
  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.css']
})
export class ClientCreateComponent implements OnInit {

  client = <Client>{};
  availableCountries: String[];

  constructor(private clientService: ClientService,
              private countryService: CountryService,
              private notificationService: NotificationService,
              private router: Router) {
  }

  createClient() {
    this.clientService.create(this.client).subscribe(
      successData => {
        this.router.navigateByUrl('/clients');
      },
      errorData => {}
    );
  }

  ngOnInit() {
    this.notificationService.clearError();
    this.countryService.getAll().subscribe(data => {
      this.availableCountries = data;
    });
  }
}
