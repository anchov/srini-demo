import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {Client} from "./client";
import {BackendApiService} from "../app-core/backend-api.service";

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private clientsUrl: string;

  constructor(private backendApiService: BackendApiService) {
    this.clientsUrl = '/clients';
  }

  public findAll(): Observable<Client[]> {
    return this.backendApiService.get<Client[]>(this.clientsUrl);
  }

  public get(clientId: String) {
    return this.backendApiService.get<Client>(this.clientsUrl + "/" + clientId);
  }

  public create(client: Client) {
    return this.backendApiService.post<Client>(this.clientsUrl, client);
  }

  public edit(client: Client) {
    return this.backendApiService.put<Client>(this.clientsUrl, client);
  }
}
